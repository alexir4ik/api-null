<?php

require_once "bootstrap/bootstrap.php";

use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with mechanism to retrieve EntityManager in your app
$entityManager = DoctrineBootstrap::getInstance()::GetEntityManager();

return ConsoleRunner::createHelperSet($entityManager);
