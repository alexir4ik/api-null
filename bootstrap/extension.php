<?php

require_once __DIR__ . '/../vendor/autoload.php';

class ExtensionBootstrap
{
    protected static $whoops;

    protected static $instance;

    public function __construct()
    {
        $environment = getenv('APP_ENV');
        /**
         * Register the error handler.
         */
        $whoops = new \Whoops\Run();

        if ($environment !== 'production') {
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
        } else {
            $whoops->pushHandler(function ($e) {
                echo 'Todo: Friendly error page and send an email to the developer';
            });
        }
        self::$whoops = $whoops->register();
    }

    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        self::$instance = new self();

        return self::$instance;
    }

    public static function GetWhoops()
    {
        return self::getInstance()::$whoops;
    }
}
