<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\Validate\Validator;

/**
 * Register routing.
 */
$dispatcher = \FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $route) {
    $route->addRoute('GET', '/feedback/create', '\App\Controllers\FeedbackController/create');
    $route->addRoute('POST', '/feedback/create', '\App\Controllers\FeedbackController/store');
    $route->addRoute('GET', '/feedbacks', '\App\Controllers\FeedbackController/read');
    $route->addRoute('GET', '/feedback[?id=...]', '\App\Controllers\FeedbackController/readOne');
    $route->addRoute('GET', '/feedback/delete[?id=...]', '\App\Controllers\FeedbackController/delete');
    $route->addRoute('GET', '/feedback/update[?id=...]', '\App\Controllers\FeedbackController/update');
    $route->addRoute('POST', '/feedback/update[?id=...]', '\App\Controllers\FeedbackController/update');
    $route->addRoute('GET', '/bookings', '\App\Controllers\BookingController/read');
    $route->addRoute('GET', '/booking[?id=...]', '\App\Controllers\BookingController/readOne');
    $route->addRoute('GET', '/booking/create', '\App\Controllers\BookingController/create');
    $route->addRoute('POST', '/booking/create', '\App\Controllers\BookingController/store');
    $route->addRoute('POST', '/booking/update[?id=...]', '\App\Controllers\BookingController/update');
    $route->addRoute('GET', '/booking/delete[?id=...]', '\App\Controllers\BookingController/delete');
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case \FastRoute\Dispatcher::NOT_FOUND:
        echo '404 Not Found';
        break;
    case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        echo '405 Method Not Allowed';
        break;
    case \FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        list($class, $method) = explode("/", $handler, 2);
        $requestValidated = Validator::validate($class, $httpMethod);
        $container = new \DI\ContainerBuilder();
        $controller = $container->build()->get($class);
        $controller->{$method}($requestValidated);
        //call_user_func($handler, $vars);
        break;
}
