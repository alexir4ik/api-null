<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

class RequestBootstrap
{
    protected static $request;

    protected static $instance;

    public function __construct()
    {
        self::$request = Request::createFromGlobals();
    }

    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        self::$instance = new self();

        return self::$instance;
    }

    public static function GetRequest()
    {
        return self::getInstance()::$request;
    }
}
