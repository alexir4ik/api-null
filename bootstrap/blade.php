<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Philo\Blade\Blade;

class BladeBootstrap
{
    protected static $blade;

    protected static $instance;

    public function __construct()
    {
        $views = dirname(__DIR__) . '/views';
        $cache = dirname(__DIR__) . '/cache';

        self::$blade = new Blade($views, $cache);
    }

    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        self::$instance = new self();

        return self::$instance;
    }

    public static function GetBlade()
    {
        return self::getInstance()::$blade;
    }
}
