<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Response;

class ResponseBootstrap
{
    protected static $response;

    protected static $instance;

    public function __construct()
    {
        self::$response = new Response();
    }

    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        self::$instance = new self();

        return self::$instance;
    }

    public static function GetResponse()
    {
        return self::getInstance()::$response;
    }
}
