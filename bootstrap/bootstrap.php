<?php

error_reporting(E_ALL);

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/extension.php';

require_once __DIR__ . '/blade.php';
require_once __DIR__ . '/request.php';
require_once __DIR__ . '/response.php';
require_once __DIR__ . '/doctrine.php';

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(dirname(__DIR__));
$dotenv->load();

BladeBootstrap::getInstance();
RequestBootstrap::getInstance();
DoctrineBootstrap::getInstance();
ExtensionBootstrap::getInstance();

require_once __DIR__ . '/router.php';
