<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class DoctrineBootstrap
{
    protected static $entityManager;

    protected static $instance;

    public function __construct()
    {
        $isDevMode = true;
        $paths = [dirname(__DIR__) . DIRECTORY_SEPARATOR . 'app/Models'];
        $connectionParams = [
            'driver' => 'pdo_mysql',
            'dbname' => getenv('DB_NAME'),
            'user' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
            'host' => getenv('DB_HOST'),
            'port' => getenv('DB_PORT'),
        ];
        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
        self::$entityManager = EntityManager::create($connectionParams, $config);
    }

    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        self::$instance = new self();

        return self::$instance;
    }

    public static function GetEntityManager()
    {
        return self::getInstance()::$entityManager;
    }
}
