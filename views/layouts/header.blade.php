<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EirnnnWorld - your beauty</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- JS-->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="././ajax.js"></script>

</head>
<body>
    <!--  Header -->
<header class="site-navbar py-1" role="banner">

    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col-6 col-xl-2" data-aos="fade-down">
                <h1 class="mb-0"><a href="" class="text-black h2 mb-0">EirnnnWorld</a></h1>
            </div>
            <div class="col-10 col-md-8 d-none d-xl-block" data-aos="fade-down">
                <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

                    <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                        <li>
                            <a href="">Home</a>
                        </li>
                        <li><a href="">Book Online</a></li>
                        <li><a href="">Contact</a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-6 col-xl-2 text-right" data-aos="fade-down">
                <div class="d-none d-xl-inline-block">
                    <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                        <li>
                            <a href="#" class="pl-3 pr-3 text-black"><span><i class="fab fa-facebook"></i></span></a>
                        </li>
                        <li>
                            <a href="#" class="pl-3 pr-3 text-black"><span><i class="fab fa-instagram"></i></span></a>
                        </li>
                        <li>
                            <a href="#" class="pl-3 pr-3 text-black"><span><i class="fab fa-twitter"></i></span></a>
                        </li>
                    </ul>
                </div>

                <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a
                        href="#" class="site-menu-toggle js-menu-toggle text-black"><span
                            class="icon-menu h3"></span></a></div>

            </div>

        </div>
    </div>
</header>
<!-- end Header -->