<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="mb-2">
                    <h3 class="footer-heading mb-2">About EirnnnWorld</h3>
                    <p class="text-white">Our salon is the best thing that can happen to you! We give each client a
                        great mood and the
                        most positive emotions!</p>
                </div>
            </div>
            <div class="col-lg-4 mb-2 mb-lg-0">
                <div class="row mb-2">
                    <div class="col-md-12">
                        <h3 class="footer-heading mb-2 text-center">Quick Menu</h3>
                    </div>
                    <div class="col-md-12 text-center">
                        <ul class="list-unstyled">
                            <li><a href="">Home</a></li>
                            <li><a href="">Book Online</a></li>
                            <li><a href="">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-3 mt-3 text-center text-white">
            <div class="col-md-12">
                <div class="mb-3">
                    <a href="#" class="pl-0 pr-3"><span><i class="fab fa-facebook"></i></span></a>
                    <a href="#" class="pl-0 pr-3"><span><i class="fab fa-instagram"></i></span></a>
                    <a href="#" class="pl-0 pr-3"><span><i class="fab fa-twitter"></i></span></a>
                </div>
            </div>

        </div>
    </div>
</footer>
<!--end  Footer-->
</body>
</html>