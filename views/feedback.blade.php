<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EirnnnWorld - your beauty</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>
    <div class="site-section bg-light mt-4">
        <div class="container">
            <div class="row">
                <div class="col-md-7 mb-5">
                    <!-- Feedback form -->
                    <form action="/feedback/create" method="post" class="p-5 bg-white" id='feedback'>
                        <div class="row form-group">
                            <!-- First and Last name user -->
                            <div class="col-md-6 mb-3 mb-md-0">
                                <label class="text-black" for="fname">First Name</label>
                                <input type="text" id="fname" name="fname" class="form-control" value="">
                            </div>
                            <div class="col-md-6">
                                <label class="text-black" for="lname">Last Name</label>
                                <input type="text" id="lname" name="lname" class="form-control " value="">
                            </div>
                        </div>
                        <!-- User's phone number -->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="phone">Mobile phone</label>
                                <input type="tel" id="phone" name="phone" class="form-control " value="">
                            </div>
                        </div>
                        <!-- User's Email address -->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="email">Email</label>
                                <input type="text" id="email" name="email" class="form-control " value="">
                            </div>
                        </div>
                        <!-- User's comment -->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="message">Message</label>
                                <textarea name="message" id="message" cols="30" rows="7" class="form-control"
                                    placeholder="Write your notes or questions here..."></textarea>
                            </div>
                        </div>
                        <!-- Button for submitting the form -->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <input name='submit' type="submit" value="Send Message"
                                    class="btn btn-primary py-2 px-4 text-white">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>