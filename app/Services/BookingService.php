<?php

namespace App\Services;

use App\Repositories\BookingRepository;

class BookingService extends BaseService
{
    public $response = [];

    public function __construct(BookingRepository $repoBooking, ClientService $clientService)
    {
        $this->repoBooking = $repoBooking;
        $this->clientService = $clientService;
    }

    public function create($data)
    {
        $client = $this->clientService->create($data);
        $data['client_id'] = $client->getId();

        return $this->repoBooking->create($data);
    }

    public function createBooking($requestValidated)
    {
        if (!empty($requestValidated['errors'])) {
            $responseBooking = [
                'status' => '404',
                'errors' => $requestValidated['errors'],
                'booking' => [],
            ];

            return $responseBooking;
        }

        $this->create($requestValidated['data']);

        if (empty($this->create($requestValidated['data']))) {
            $responseBooking = [
                'status' => '503',
                'errors' => ['It is impossible to create booking.'],
                'booking' => [],
            ];

            return $responseBooking;
        }

        $responseBooking = [
            'status' => '201',
            'errors' => [],
            'booking' => [],
        ];

        return $responseBooking;
    }

    public function readAllBooking()
    {
        $bookingArrays = [];
        $bookings = $this->repoBooking->findAll();

        if (empty($bookings)) {
            $responseBooking = [
                'status' => '404',
                'errors' => ['Data from the table \'booking\' was not found'],
                'booking' => $bookingArrays,
            ];

            return $responseBooking;
        }

        foreach ($bookings as $booking) {
            $booking = [
                'id' => $booking->getId(),
                'client_id' => $booking->getClientId(),
                'reserved_time' => $booking->getReservedTime(),
                'date' => $booking->getDate(),
                'created_at' => $booking->getCreatedAt(),
            ];

            $client = $this->repoBooking->getClient($booking['id']);
            $booking = array_merge($booking, $client);
            array_push($bookingArrays, $booking);
        }

        $responseBooking = [
            'status' => '200',
            'errors' => [],
            'booking' => $bookingArrays,
        ];
        return $responseBooking;
    }

    public function readOneBooking($request)
    {
        if (empty($request->get('id'))) {
            $responseBooking = [
                'status' => '404',
                'errors' => ['There is not enough data to complete the request'],
                'booking' => [],
            ];
            return $responseBooking;
        }

        $booking = $this->repoBooking->findId($request->get('id'));

        if (empty($booking)) {
            $responseBooking = [
                'status' => '404',
                'errors' => ['The booking with id=' . $request->get('id') . ' does not exist'],
                'booking' => [],
            ];
            return $responseBooking;
        }

        $booking = [
            'id' => $booking->getId(),
            'client_id' => $booking->getClientId(),
            'reserved_time' => $booking->getReservedTime(),
            'date' => $booking->getDate(),
            'created_at' => $booking->getCreatedAt(),
        ];

        $client = $this->repoBooking->getClient($request->get('id'));
        $booking = array_merge($booking, $client);

        $responseBooking = [
            'status' => '200',
            'errors' => [],
            'booking' => $booking,
        ];

        return $responseBooking;
    }

    public function update($bookingId, $data)
    {
        $booking = $this->repoBooking->findId($bookingId);
        $clientId = $booking->getClientId();
        $booking = $this->repoBooking->update($bookingId, $data);
        $client = $this->clientService->update($clientId, $data);

        if ($booking && $client) {
            return true;
        }
        return false;
    }

    public function updateBooking($request, $requestValidated)
    {
        if (empty($request->get('id')) || empty($requestValidated['data'])) {
            $responseBooking = [
                'status' => '404',
                'errors' => [$requestValidated['errors'] ?? 'There is not enough data to complete the request'],
                'booking' => [],
            ];

            return $responseBooking;
        }

        if (empty($this->repoBooking->findId($request->get('id')))) {
            $responseBooking = [
                'status' => '404',
                'errors' => ['The booking with id=' . $request->get('id') . ' does not exist'],
                'booking' => [],
            ];

            return $responseBooking;
        }

        $this->update($request->get('id'), $requestValidated['data']);

        $responseBooking = [
            'status' => '200',
            'errors' => [],
            'booking' => [],
        ];

        return $responseBooking;
    }

    public function deleteBooking($request)
    {
        if (empty($request->get('id'))) {
            $responseBooking = [
                'status' => '404',
                'errors' => ['There is not enough data to complete the request'],
                'booking' => [],
            ];

            return $responseBooking;
        }

        if (empty($this->repoBooking->findId($request->get('id')))) {
            $responseBooking = [
                'status' => '404',
                'errors' => ['The booking with id=' . $request->get('id') . ' does not exist'],
                'booking' => [],
            ];

            return $responseBooking;
        }

        $this->clientService->delete($this->repoBooking->findId($request->get('id'))->getClientId());
        $this->repoBooking->delete($request->get('id'));

        $responseBooking = [
            'status' => '200',
            'errors' => [],
            'booking' => [],
        ];

        return $responseBooking;
    }

    public function getClient($bookingId)
    {
        return $this->repoBooking->getClient($bookingId);
    }
}
