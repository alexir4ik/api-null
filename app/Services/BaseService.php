<?php

namespace App\Services;

abstract class BaseService
{
    public $repo;

    public function findAll()
    {
        return $this->repo->findAll();
    }

    public function create($data)
    {
        return $this->repo->create($data);
    }

    public function findId($id)
    {
        return $this->repo->findId($id);
    }

    public function update($id, $data)
    {
        $this->repo->update($id, $data);
    }

    public function delete($id)
    {
        $this->repo->delete($id);
    }
}
