<?php

namespace App\Services;

use App\Repositories\FeedbackRepository;

class FeedbackService extends BaseService
{
    public $response = [];

    public function __construct(FeedbackRepository $repoFeedback)
    {
        $this->repoFeedback = $repoFeedback;
    }

    public function readAllFeedback()
    {
        $feedbackArrays = [];
        $feedbacks = $this->repoFeedback->findAll();

        if (empty($feedbacks)) {
            $responseFeedback = [
                'status' => '404',
                'errors' => ['Data from the table \'feedback\' was not found'],
                'feedback' => [],
            ];
            return $responseFeedback;
        }

        foreach ($feedbacks as $feedback) {
            array_push($feedbackArrays, [
                'id' => $feedback->getId(),
                'lname' => $feedback->getLName(),
                'fname' => $feedback->getFName(),
                'phone' => $feedback->getPhone(),
                'email' => $feedback->getEmail(),
                'message' => $feedback->getMessage(),
                'received' => $feedback->getReceived(),
            ]);
        }

        return $responseFeedback = [
            'status' => '200',
            'errors' => [],
            'feedback' => $feedbackArrays,
        ];
    }

    public function readOneFeedback($request)
    {
        if (empty($request->get('id'))) {
            $responseFeedback = [
                'status' => '404',
                'errors' => ['There is not enough data to complete the request'],
                'feedback' => [],
            ];
            return $responseFeedback;
        }

        $feedback = $this->repoFeedback->findId($request->get('id'));

        if (empty($feedback)) {
            $responseFeedback = [
                'status' => '404',
                'errors' => ['The feedback does not exist'],
                'feedback' => [],
            ];
            return $responseFeedback;
        }

        $responseFeedback = [
            'status' => '200',
            'errors' => [],
            'feedback' => [
                'lname' => $feedback->getLName(),
                'fname' => $feedback->getFName(),
                'phone' => $feedback->getPhone(),
                'email' => $feedback->getEmail(),
                'message' => $feedback->getMessage(),
                'received' => $feedback->getReceived(),
            ],
        ];
        return $responseFeedback;
    }

    public function createFeedback($requestValidated)
    {
        if (!empty($requestValidated['errors'])) {
            $responseFeedback = [
                'status' => '404',
                'errors' => $requestValidated['errors'],
                'feedback' => [],
            ];
            return $responseFeedback;
        }
        $this->repoFeedback->create($requestValidated['data']);

        if (empty($this->repoFeedback->create($requestValidated['data']))) {
            $responseFeedback = [
                'status' => '503',
                'errors' => ['It is impossible to create feedback.'],
                'feedback' => [],
            ];

            return $responseFeedback;
        }

        $responseFeedback = [
            'status' => '201',
            'errors' => [],
            'feedback' => [],
        ];

        return $responseFeedback;
    }

    public function updateFeedback($request, $requestValidated)
    {
        if (empty($request->get('id')) || empty($requestValidated['data'])) {
            $responseFeedback = [
                'status' => '404',
                'errors' => [$requestValidated['errors'] ?? 'There is not enough data to complete the request'],
                'feedback' => [],
            ];
            return $responseFeedback;
        }

        if (empty($this->repoFeedback->findId($request->get('id')))) {
            $responseFeedback = [
                'status' => '404',
                'errors' => ['The feedback with id=' . $request->get('id') . ' does not exist'],
                'feedback' => [],
            ];
            return $responseFeedback;
        }

        $this->repoFeedback->update($request->get('id'), $requestValidated['data']);
        $responseFeedback = [
            'status' => '200',
            'errors' => [],
            'feedback' => [],
        ];

        return $responseFeedback;
    }

    public function deleteFeedback($request)
    {
        if (empty($request->get('id'))) {
            $responseFeedback = [
                'status' => '404',
                'errors' => ['There is not enough data to complete the request'],
                'feedback' => [],
            ];
            return $responseFeedback;
        }

        if (empty($this->repoFeedback->findId($request->get('id')))) {
            $responseFeedback = [
                'status' => '404',
                'errors' => ['The feedback with id=' . $request->get('id') . ' does not exist'],
                'feedback' => [],
            ];
            return $responseFeedback;
        }

        $this->repoFeedback->delete($request->get('id'));
        $responseFeedback = [
            'status' => '200',
            'errors' => [],
            'feedback' => [],
        ];

        return $responseFeedback;
    }
}
