<?php

namespace App\Validate;

class ValidateBooking
{
    public static function validate($data)
    {
        $validator = new Validation();

        if (!empty($data['reserved_time'])) {
            $validator->name('reserved_time')->value($data['reserved_time'])->pattern('int');
        }

        if (!empty($data['date'])) {
            $validator->name('date')->value($data['date'])->pattern('date_ymd');
        }

        if (empty($validator->getErrors())) {
            return [];
        }
        return $validator->displayErrors();
    }
}
