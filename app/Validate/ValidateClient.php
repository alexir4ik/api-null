<?php

namespace App\Validate;

class ValidateClient
{
    public static function validate($data)
    {
        $validator = new Validation();

        if (!empty($data['email'])) {
            $validator->name('email')->value($data['email'])->is_email($data['email']);
        }

        if (!empty($data['phone'])) {
            $validator->name('phone')->value($data['phone'])->pattern('tel');
        }

        if (!empty($data['message'])) {
            $validator->name('message')->value($data['message'])->pattern('text')->min(5);
            $validator->name('message')->value($data['message'])->pattern('text')->max(255);
        }

        if (!empty($data['lname'] && $data['fname'])) {
            $validator->name('lname')->value($data['lname'])->pattern('text')->min(5);
            $validator->name('lname')->value($data['lname'])->pattern('text')->max(100);
            $validator->name('fname')->value($data['fname'])->pattern('text')->min(5);
            $validator->name('fname')->value($data['fname'])->pattern('text')->max(255);
        }

        if (empty($validator->getErrors())) {
            return [];
        }
        return $validator->displayErrors();
    }
}
