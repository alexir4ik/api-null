<?php

namespace App\Validate;

class Validator
{
    public static function validate($className, $method)
    {
        $response = [];

        if ($method == 'POST') {
            $className = preg_match('/^.*\\\(\w+)Controller$/', $className, $classMatch);

            $validatorClass = $classMatch[1];
            $validateClient = self::validateClient($validatorClass);
            $validatorClass = '\App\Validate\Validate' . $validatorClass;
            $validator = new $validatorClass();

            $require = self::required(\RequestBootstrap::getInstance()::getRequest()->request->all());
            $response = $validator->validate(\RequestBootstrap::getInstance()::getRequest()->request->all());

            if (empty($require) && empty($response) && empty($validateClient)) {
                $requestValidated['data'] = \RequestBootstrap::getInstance()::getRequest()->request->all();
            }

            if (!empty($require) || !empty($response) || !empty($validateClient)) {
                $requestValidated['errors'] = array_merge($require, $response, $validateClient);
            }

            return $requestValidated;
        }
    }

    private static function required($request)
    {
        $validator = new Validation();
        foreach ($request as $key => $value) {
            $required = $validator->name($key)->value($value)->required();
        }

        if (empty($validator->getErrors())) {
            return [];
        }
        return $validator->displayErrors();
    }

    private static function validateClient($validatorClass)
    {
        $validateClient = [];

        if ($validatorClass === 'Booking') {
            $validator = new \App\Validate\ValidateClient();
            $validateClient = $validator->validate(\RequestBootstrap::getInstance()::getRequest()->request->all());
        }

        return $validateClient;
    }
}
