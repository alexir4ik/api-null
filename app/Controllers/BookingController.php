<?php

namespace App\Controllers;

use App\Services\BookingService;
use App\Services\ClientService;

class BookingController extends BaseController
{
    public function __construct(ClientService $client, BookingService $booking)
    {
        $this->client = $client;
        $this->booking = $booking;
    }

    public function readOne()
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'booking' => $responseData
        ) = $this->booking->readOneBooking(\RequestBootstrap::getInstance()::getRequest());

        $this->response($responseData, $errors, $status);
    }

    public function read()
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'booking' => $responseData
        ) = $this->booking->readAllBooking();

        $this->response($responseData, $errors, $status);
    }

    public function store($requestValidated)
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'booking' => $responseData
        ) = $this->booking->createBooking($requestValidated);

        $this->response($responseData, $errors, $status);
    }

    public function update($requestValidated)
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'booking' => $responseData
        ) = $this->booking->updateBooking(\RequestBootstrap::getInstance()::getRequest(), $requestValidated);

        $this->response($responseData, $errors, $status);
    }

    public function delete()
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'booking' => $responseData
        ) = $this->booking->deleteBooking(\RequestBootstrap::getInstance()::getRequest());

        $this->response($responseData, $errors, $status);
    }
}
