<?php

namespace App\Controllers;

use App\Services\FeedbackService;

class FeedbackController extends BaseController
{
    public function __construct(FeedbackService $feedback)
    {
        $this->feedback = $feedback;
    }

    public function read()
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'feedback' => $responseData
        ) = $this->feedback->readAllFeedback();

        $this->response($responseData, $errors, $status);
    }

    public function readOne()
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'feedback' => $responseData
        ) = $this->feedback->readOneFeedback(\RequestBootstrap::getInstance()::getRequest());

        $this->response($responseData, $errors, $status);
    }

    public function store($requestValidated)
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'feedback' => $responseData
        ) = $this->feedback->createFeedback($requestValidated);

        $this->response($responseData, $errors, $status);
    }

    public function update($requestValidated)
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'feedback' => $responseData
        ) = $this->feedback->updateFeedback(\RequestBootstrap::getInstance()::getRequest(), $requestValidated);

        $this->response($responseData, $errors, $status);
    }

    public function delete()
    {
        list(
            'status' => $status,
            'errors' => $errors,
            'feedback' => $responseData
        ) = $this->feedback->deleteFeedback(\RequestBootstrap::getInstance()::getRequest());

        $this->response($responseData, $errors, $status);
    }
}
