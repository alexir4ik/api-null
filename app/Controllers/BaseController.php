<?php

namespace App\Controllers;

class BaseController
{
    public function response($responseData, $errors, $status)
    {
        $method = \RequestBootstrap::getInstance()::getRequest()->getMethod();
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: " . $method);

        http_response_code(200);

        $errors = $errors ?? [];
        $responseData = $responseData ?? [];
        $status = $status ?? 0;

        echo json_encode([
            'data' => $responseData,
            'errors' => $errors,
            'status' => (int) $status,
        ]);
    }
}
