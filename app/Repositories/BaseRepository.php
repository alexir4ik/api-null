<?php

namespace App\Repositories;

use Doctrine\ORM\Mapping\Entity;

class BaseRepository
{
    private $nameModel;

    public function __construct(Entity $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        $model = $this->model->create($data);
        \DoctrineBootstrap::getInstance()::GetEntityManager()->persist($this->model);
        \DoctrineBootstrap::getInstance()::GetEntityManager()->flush();
        return $model;
    }

    public function findAll($nameModel)
    {
        return \DoctrineBootstrap::getInstance()::GetEntityManager()->getRepository($nameModel)->findAll();
    }
}
