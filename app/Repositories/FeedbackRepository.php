<?php

namespace App\Repositories;

use App\Models\Feedback;

class FeedbackRepository
{
    private $nameModel = '\App\Models\Feedback';

    public function __construct(Feedback $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        $this->model->setLName($data['lname']);
        $this->model->setFName($data['fname']);
        $this->model->setPhone($data['phone']);
        $this->model->setEmail($data['email']);
        $this->model->setMessage($data['message']);
        $this->model->setReceived(new \DateTime(date('Y-m-d H:i:s')));
        \DoctrineBootstrap::getInstance()::GetEntityManager()->persist($this->model);
        \DoctrineBootstrap::getInstance()::GetEntityManager()->flush();
        return $this->model;
    }

    public function findAll()
    {
        return  \DoctrineBootstrap::getInstance()::GetEntityManager()->getRepository('\App\Models\Feedback')->findAll();
    }

    public function findId($id)
    {
        $feedback = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Feedback', $id);
        return $feedback;
    }

    public function update(int $id, array $data)
    {
        $feedback = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Feedback', $id);
        $feedback->setFName($data['fname'] ?? $feedback->getFName());
        $feedback->setLName($data['lname'] ?? $feedback->getLName());
        $feedback->setPhone($data['phone'] ?? $feedback->getPhone());
        $feedback->setEmail($data['email'] ?? $feedback->getEmail());
        $feedback->setMessage($data['message'] ?? $feedback->getMessage());

        \DoctrineBootstrap::getInstance()::GetEntityManager()->flush();
    }

    public function delete($id)
    {
        $db = \DoctrineBootstrap::getInstance()::GetEntityManager();
        $feedback = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Feedback', $id);
        $db->remove($feedback);
        $db->flush();
    }
}
