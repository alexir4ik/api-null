<?php

namespace App\Repositories;

use App\Models\Booking;

class BookingRepository
{
    private $nameModel = '\App\Models\Booking';

    public function __construct(Booking $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        $this->model->setClientId($data['client_id']);
        $this->model->setReservedTime($data['reserved_time']);
        $this->model->setDate(\DateTime::createFromFormat('Y-m-d', $data['date']));
        $this->model->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));

        \DoctrineBootstrap::getInstance()::GetEntityManager()->persist($this->model);
        \DoctrineBootstrap::getInstance()::GetEntityManager()->flush();

        return $this->model;
    }

    public function findAll()
    {
        return  \DoctrineBootstrap::getInstance()::GetEntityManager()->getRepository('\App\Models\Booking')->findAll();
    }

    public function findId($id)
    {
        return \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Booking', $id);
    }

    public function update(int $id, array $data)
    {
        $booking = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Booking', $id);
        $booking->setClientId($data['client_id'] ?? $booking->getClientId());
        $booking->setReservedTime($data['reserved_time'] ?? $booking->getReservedTime());
        $booking->setDate(\DateTime::createFromFormat('Y-m-d', $data['date']) ?? $booking->getDate());
        $booking->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));
        \DoctrineBootstrap::getInstance()::GetEntityManager()->flush();
    }

    public function delete($id)
    {
        $db = \DoctrineBootstrap::getInstance()::GetEntityManager();
        $booking = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Booking', $id);
        $db->remove($booking);
        $db->flush();
    }

    public function getClient($bookingId)
    {
        $client = [];

        $booking = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Booking', $bookingId);

        $clientId = $booking->getClientId();
        $client = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Client', $clientId);

        if (!empty($client)) {
            $client = [
                'lname' => $client->getLName(),
                'fname' => $client->getFName(),
                'email' => $client->getEmail(),
                'phone' => $client->getPhone(),
                'message' => $client->getMessage(),
            ];
        }
        return $client;
    }
}
