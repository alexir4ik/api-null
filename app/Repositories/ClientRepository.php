<?php

namespace App\Repositories;

use App\Models\Client;

class ClientRepository
{
    private $nameModel = '\App\Models\Client';

    public function __construct(Client $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        $this->model->setFName($data['fname']);
        $this->model->setLName($data['lname']);
        $this->model->setPhone($data['phone']);
        $this->model->setEmail($data['email']);
        $this->model->setMessage($data['message']);
        $this->model->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        \DoctrineBootstrap::getInstance()::GetEntityManager()->persist($this->model);
        \DoctrineBootstrap::getInstance()::GetEntityManager()->flush();

        return $this->model;
    }

    public function findAll()
    {
        return  \DoctrineBootstrap::getInstance()::GetEntityManager()->getRepository('\App\Models\Client')->findAll();
    }

    public function findId($id)
    {
        $client = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Client', $id);
        return $client;
    }

    public function update(int $id, array $data)
    {
        $client = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Client', $id);
        $client->setFName($data['fname'] ?? $client->getFName());
        $client->setLName($data['lname'] ?? $client->getLName());
        $client->setPhone($data['phone'] ?? $client->getPhone());
        $client->setEmail($data['email'] ?? $client->getEmail());
        $client->setMessage($data['message'] ?? $client->getMessage());
        $client->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));
        \DoctrineBootstrap::getInstance()::GetEntityManager()->flush();
    }

    public function delete($id)
    {
        $db = \DoctrineBootstrap::getInstance()::GetEntityManager();
        $client = \DoctrineBootstrap::getInstance()::GetEntityManager()->find('\App\Models\Client', $id);
        $db->remove($client);
        $db->flush();
    }
}
