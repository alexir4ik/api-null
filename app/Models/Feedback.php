<?php

namespace App\Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="feedback")
 */
class Feedback
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $lname;
    /**
     * @ORM\Column(type="string")
     */
    private $fname;
    /**
     * @ORM\Column(type="string")
     */
    private $email;
    /**
     * @ORM\Column(type="string")
     */
    private $phone;
    /**
     * @ORM\Column(type="string")
     */
    private $message;
    /**
     * @ORM\Column(type="datetime")
     */
    private $received;

    public function getId()
    {
        return $this->id;
    }

    public function getLName()
    {
        return $this->lname;
    }

    public function setLName($lname)
    {
        $this->lname = $lname;
    }

    public function getFName()
    {
        return $this->fname;
    }

    public function setFName($fname)
    {
        $this->fname = $fname;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getReceived()
    {
        return $this->received;
    }

    public function setReceived($received)
    {
        $this->received = $received;
    }
}
